
MSE =

  Columns 1 through 8

    0.5792    0.5792    0.5792    0.5792    0.5792    0.5792    0.5792    0.5792

  Columns 9 through 16

    0.5792    0.5792    0.5792    0.5792    0.5792    0.5792    0.5793    0.5792

  Columns 17 through 20

    0.5792    0.5792    0.5792    0.5792


NC =

  Columns 1 through 8

    0.9996    0.9985    0.9996    0.9987    0.9997    0.9999    0.9999    0.9988

  Columns 9 through 16

    0.9997    0.9999    0.9982    0.9997    0.9999    0.9999    0.9997    0.9993

  Columns 17 through 20

    0.9985    0.9985    0.9994    0.9980


PSNR =

  Columns 1 through 8

   50.5026   50.5024   50.5024   50.5024   50.5022   50.5026   50.5024   50.5021

  Columns 9 through 16

   50.5023   50.5024   50.5025   50.5028   50.5023   50.5025   50.5021   50.5026

  Columns 17 through 20

   50.5024   50.5023   50.5024   50.5025


corr_coef =

  Columns 1 through 8

    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028

  Columns 9 through 16

    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028    0.8028

  Columns 17 through 20

    0.8028    0.8028    0.8028    0.8028

