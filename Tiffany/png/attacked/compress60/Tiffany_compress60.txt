
MSE =

  Columns 1 through 8

    0.1329    0.1335    0.1337    0.1334    0.1330    0.1333    0.1327    0.1335

  Columns 9 through 16

    0.1337    0.1330    0.1336    0.1333    0.1334    0.1328    0.1330    0.1334

  Columns 17 through 20

    0.1328    0.1337    0.1334    0.1335


PSNR =

  Columns 1 through 8

   56.8953   56.8763   56.8710   56.8804   56.8929   56.8828   56.9028   56.8748

  Columns 9 through 16

   56.8708   56.8908   56.8731   56.8832   56.8778   56.8987   56.8936   56.8790

  Columns 17 through 20

   56.8978   56.8693   56.8796   56.8744


corr_coef =

  Columns 1 through 8

    0.8603    0.8599    0.8600    0.8603    0.8602    0.8597    0.8603    0.8601

  Columns 9 through 16

    0.8596    0.8601    0.8599    0.8602    0.8601    0.8603    0.8603    0.8599

  Columns 17 through 20

    0.8602    0.8596    0.8603    0.8601

